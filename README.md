# Warfork HUDScript Preprocessor

This is an external "preprocessor" that compiles down to Warfork/Warsow HUDScript, in an attempt to make HUD Scripting easier.

As suggested by [Panda](https://pandaptable.moe), this project can officially be abbreviated as HudPP.

## Documentation
This is temporary. Once development has progressed further, these docs will updated.

### Functions
Important Note: Be very careful with your operations. They are evaluated right to left in HUDScript, and there's nothing this preprocessor can do about it.
| Symbol | Definition | Example |
|--------|------------|---------|
| `.` | Allows for the combination of multiple statements into one. | `(. (call setcursor 20 20) (call movecursor 30 0))` |
| `+` | Returns the sum of all arguments. | `(+ 1 2 3)` |
| `-` | Returns the difference between the first argument and the sum of the proceeding arguments. | `(- 1 2 3)` |
| `*` | Returns the product of all arguments. | `(* 1 2 3)` |
| `/` | Returns the quotient of the first argument and the second argument. See `=` for limitations. | `(/ 1 2)` |
| `&` | Bitwise and. See `=` for limitations. | `(& 0 1)` |
| `|` | Bitwise or. See `=` for limitations. | `(| 0 1)` |
| `^` | Logical xor. See `=` for limitations. | `(^ 0 1)` |
| `=` | Returns the equality of 2 arguments. Note that the first argument must be a plain value due to WarFork's way of handling order of operations. | `(= 1 (- 3 2))` |
| `!=` | Logical not-equal. See `=` for limitations. | `(!= 0 1)` |
| `>` | Greater than. See `=` for limitations. | `(> 0 1)` |
| `>=` | Greater or equal to. See `=` for limitations. | `(>= 0 1)` |
| `<` | Less than. See `=` for limitations. | `(< 0 1)` |
| `<=` | Less than or equal to. See `=` for limitations. | `(<= 0 1)` |
| `and` | Logical and. See `=` for limitations. | `(and 0 1)` |
| `call` | Calls a HUDScript function. | `(call setCursor (+ 2 3) 4)` |
| `def` | (Re)defines a variable. | `(def x (+ 2 3))` |
| `func` | Directly assigns a variable to a Sexpr. This can be combined with `run`. | `(func movecursorx (call movecursor 10 0))` |
| `if` | Evaluates to an if statement in HUDScript. This supports an "if-else" case. | `(if (= 1 1) (call setCursor 0 0))` OR `(if (= 1 1) (call setCursor 0 0) (call setCursor 1 1))` |
| `include` | Evaluates another file in your include directory inplace. | `(include other.scm)` |
| `nil` | No-op. | `(nil)` |
| `or` | Logical or. See `=` for limitations. | `(or 0 1)` |
| `run` | Runs a Sexpr in the variable stack. | `(run movecursorx)` |
| `say` | Prints the arguments given. This is for debugging purposes. | `(say x 1 "Haii! :3")` |
