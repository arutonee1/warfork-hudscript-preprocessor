import argparse
from io import TextIOWrapper
from pathlib import Path
import re

from src.sexpr import *
from src.run import *


def main() -> None:
    input_path, output_path, include_path = handle_arguments()
    with open(input_path, 'r') as infile:
        with open(output_path, 'w') as outfile:
            outfile.write(parse(infile, include_path))


def parse(content: TextIOWrapper, include_path: Path) -> str:
    text = '(' + content.read().strip().replace('\n', ' ').replace('\t', ' ') + ')'
    terms = get_sexpr_terms(text)
    return run(terms, include_path)


def handle_arguments() -> tuple[str, str, Path]:
    parser = argparse.ArgumentParser(
            prog='Warfork HUDScript Preprocessor',
            epilog='See https://gitlab.com/arutonee1/warfork-hudscript-preprocessor for more info.')
    parser.add_argument('path')
    parser.add_argument('-o', dest='output', required=True)
    parser.add_argument('-I', dest='include_path', default='.')
    args = parser.parse_args()
    include_path = Path(args.include_path)
    if not include_path.is_dir():
        print('include_path is not a directory.')
        exit(1)
    return (args.path, args.output, include_path)


if __name__ == '__main__':
    main()
