from pathlib import Path

from src.sexpr import *


def num(s) -> int | float:
    if type(s) is not String: return s
    if '.' in s: return float(s)
    return int(s)

def D_OPERATION(string):
    def op(a, v):
        parts = []
        for arg in a:
            match arg:
                case Name(): parts.append(num(v[arg.val]))
                case Sexpr(): parts.append(run([arg]))
                case _: parts.append(num(arg.val))
        return (' ' + string + ' ').join(map(str, parts))
    return op
def D_BIN_OP(string, name):
    def op(a, v):
        if len(a) != 2: return f'(INVALID USAGE OF {name})'
        first_half = None
        match a[0]:
            case Name(): first_half = str(v[a[0].val])
            case _: first_half = str(a[0].val)
        second_half = None
        match a[1]:
            case Name(): second_half = str(v[a[1].val])
            case Sexpr(): second_half = run([a[1]])
            case _: second_half = str(a[1].val)
        return f'{first_half} {string} {second_half}'
    return op
def F_ADD(a, v):
    return D_OPERATION('+')(a, v)
def F_AND(a, v):
    return D_BIN_OP('&&', 'and')(a, v)
def F_BAND(a, v):
    return D_BIN_OP('&', '&')(a, v)
def F_BOR(a, v):
    return D_BIN_OP('|', '|')(a, v)
def F_CALL(a, v):
    if len(a) == 0: return '(NO FUNCTION TO call)'
    if len(a) == 1: return str(a[0].val) + '\n'
    output = str(a[0].val)
    for arg in a[1:]:
        match arg:
            case Name(): output += ' ' + str(v[arg.val])
            case Sexpr(): output += ' ' + run([arg])
            case _: output += ' ' + str(arg.val)
    output += '\n'
    return output
def F_DEF(a, v):
    if len(a) != 2: return '(INVALID USAGE OF def)'
    value = None
    match a[1]:
        case Name(): value = str(v[a[1].val])
        case Sexpr(): value = run([a[1]])
        case _: value = str(a[1].val)
    v[a[0].val] = value
    return ''
def F_DIVIDE(a, v):
    return D_BIN_OP('/', '/')(a, v)
def F_DOT(a, v):
    parts = []
    for arg in a:
        match arg:
            case Name(): parts.append(str(v[arg.val]))
            case Sexpr(): parts.append(run([arg]))
            case _: parts.append(str(arg.val))
    return ''.join(parts)
def F_EQUAL(a, v):
    return D_BIN_OP('==', '=')(a, v)
def F_FUNC(a, v):
    if len(a) != 2: return '(INVALID USAGE OF func)'
    v[a[0].val] = a[1]
    return ''
def F_GREATER(a, v):
    return D_BIN_OP('>', '>')(a, v)
def F_GREATER_EQUAL(a, v):
    return D_BIN_OP('>=', '>=')(a, v)
def F_IF(a, v):
    l = len(a)
    if l < 2 or l > 3: return '(INVALID USAGE OF if)'
    condition = None
    match a[0]:
        case Name(): condition = str(v[a[0].val])
        case Sexpr(): condition = run([a[0]])
        case _: condition = str(a[0].val)
    if len(a) == 2: return f'if {condition}\n{run([a[1]])}\nendif\n'
    if len(a) == 3: return f'if {condition}\n{run([a[1]])}\nendif\nifnot {condition}\n{run([a[2]])}\nendif\n'
def F_INCLUDE(a, v):
    out = ''
    for arg in a:
        file = VARS['___INCLUDE'] / arg.val
        if not file.is_file(): return '(INCLUDE NOT FOUND)'
        with open(file) as f:
            text = '(' + f.read().strip().replace('\n', ' ') + ')'
            terms = get_sexpr_terms(text)
            out += run(terms)
    return out
def F_MULTIPLY(a, v):
    return D_OPERATION('*')(a, v)
def F_NIL(a, v):
    return ''
def F_NOT_EQUAL(a, v):
    return D_BIN_OP('!=', '!=')(a, v)
def F_OR(a, v):
    return D_BIN_OP('||', 'or')(a, v)
def F_RUN(a, v):
    return run([v[a[0].val]])
def F_SAY(a, v):
    strs = []
    for arg in a:
        match arg:
            case Name(): strs.append(v[arg.val])
            case Sexpr(): strs.append(run([arg]))
            case _: strs.append(str(arg.val))
    print(' '.join(map(str, strs)))
    return ''
def F_SMALLER(a, v):
    return D_BIN_OP('<', '<')(a, v)
def F_SMALLER_EQUAL(a, v):
    return D_BIN_OP('<=', '<=')(a, v)
def F_STR(a, v):
    strs = []
    for arg in a:
        match arg:
            case Name(): strs.append(v[arg.val])
            case Sexpr(): strs.append(run([arg]))
            case _: strs.append(str(arg.val))
    return ''.join(strs)
def F_SUBTRACT(a, v):
    first = None
    match a[0]:
        case Name(): first = v[a[0].val]
        case Sexpr(): first = run([a[0]])
        case _: first = str(a[0].val)
    return f'{first} - {D_OPERATION("+")(a[1:], v)}'
def F_XOR(a, v):
    return D_BIN_OP('^', '^')(a, v)


FUNCTIONS = {
    '+': F_ADD,
    '-': F_SUBTRACT,
    '*': F_MULTIPLY,
    '/': F_DIVIDE,
    '&': F_BAND,
    '|': F_BOR,
    '^': F_XOR,
    '=': F_EQUAL,
    '!=': F_NOT_EQUAL,
    '>': F_GREATER,
    '>=': F_GREATER_EQUAL,
    '<': F_SMALLER,
    '<=': F_SMALLER_EQUAL,
    '.': F_DOT,
    'and': F_AND,
    'call': F_CALL,
    'def': F_DEF,
    'func': F_FUNC,
    'if': F_IF,
    'include': F_INCLUDE,
    'nil': F_NIL,
    'or': F_OR,
    'run': F_RUN,
    'say': F_SAY,
    'str': F_STR,
}

VARS = {
    '___INCLUDE': Path('.'),
}

def run(terms: list[Token], include_path: Path=None) -> str:
    if include_path is not None:
        VARS['___INCLUDE'] = include_path
    output = ''
    for term in terms:
        name = term.head.val
        if name in FUNCTIONS:
            output += FUNCTIONS[name](term.tail, VARS)
        else:
            print(f'Function {name} is not defined.')
            exit(1)
    return output
