ESCAPE_MAPPINGS = {
    'n': '\n',
    't': '\t',
    '\\': '\\',
    '"': '"',
}


class Token: pass


class Atom(Token): pass


class Name(Atom):
    def __init__(self, val: str) -> None:
        self.val = str(val)
    def __str__(self) -> str:
        return f'NAME[{self.val}]'


class Number(Atom):
    def __init__(self, val: int | float) -> None:
        self.val = val
    def __str__(self) -> str:
        return f'NUMBER[{self.val}]'


class String(Atom):
    def __init__(self, val: str) -> None:
        self.val = str(val)
    def __str__(self) -> str:
        return f'STRING[{self.val}]'


class Sexpr(Token):
    def __init__(self, head: Name, tail: list[Token]) -> None:
        self.head = head
        self.tail = tail
    def __str__(self) -> str:
        return f'SEXPR[{self.head} {" ".join(map(str, self.tail))}]'


def get_sexpr_terms(text: str) -> list[Token]:
    i = 1
    nesting = 0
    in_str = False
    escaped = False
    terms = []
    buffer = ''
    while i < len(text):
        match text[i], nesting, in_str, escaped:
            case c, _, _, True:
                buffer += c
                escaped = False
            case '"', _, False, _:
                in_str = True
                buffer += '"'
            case '"', 0, True, _:
                in_str = False
                terms.append(parse_token(buffer + '"'))
                buffer = ''
            case '"', _, True, _:
                in_str = False
                buffer += '"'
            case '\\', _, True, _:
                escaped = True
            case c, _, True, _:
                buffer += c

            case '(', 0, False, _:
                nesting += 1
            case '(', _, False, _:
                nesting += 1
                buffer += '('
            case ')', 0, False, _:
                break
            case ')', 1, False, _:
                nesting -= 1
                terms.append(parse_token('(' + buffer + ')'))
                buffer = ''
            case ')', _, False, _:
                nesting -= 1
                buffer += ')'

            case ' ', 0, False, _:
                if len(buffer) != 0:
                    terms.append(parse_token(buffer))
                buffer = ''
            case c, _, False, _:
                buffer += c
        i += 1
    if len(buffer) != 0:
        terms.append(parse_token(buffer))
    return terms

def parse_token(text: str) -> Token:
    text = text.strip()
    match text[0]:
        case '(':
            terms = get_sexpr_terms(text)
            if len(terms) == 0: return Sexpr(Name('nil'), [])
            if len(terms) == 1: return Sexpr(terms[0], [])
            return Sexpr(terms[0], terms[1:])
        case d if d in '0123456789':
            num_type = float if '.' in text else int
            return Number(num_type(text))
        case '"':
            buffer = ''
            escaped = False
            i = 1
            while i < len(text):
                match escaped, text[i]:
                    case True, c:
                        if c in ESCAPE_MAPPINGS:
                            buffer += ESCAPE_MAPPINGS[c]
                        escaped = False
                    case False, '\\':
                        escaped = True
                    case False, '"':
                        break
                    case _:
                        buffer += text[i]
                i += 1
            return String(buffer)
        case _:
            return Name(text)
